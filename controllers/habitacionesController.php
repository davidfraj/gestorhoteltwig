<?php  
//Fichero: controllers/habitacionesController.php

//Incluir el modelo
require('models/imagenHabitacionModel.php');
require('models/habitacionModel.php');
require('models/habitacionesModel.php');

//Llamo al modelo de datos de Habitaciones, donde conectare
// con la BBDD usando sentencias SQL
$mishabitaciones=new Habitaciones();

if(isset($_GET['accion'])){
	$accion=$_GET['accion'];
}else{
	$accion='listado';
}

switch($accion){
	 case 'listado':

		$habitaciones=$mishabitaciones->listado(); //Me traer objetos de la clase Habitacion
		//Establezco los datos que quiero mostrar en la vista
		$tituloWeb='Listado de habitaciones';
		$contenidoWeb='Aqui pongo el contenido que a mi me de la gana';
		//Pasar los datos a la vista y renderizar
		echo $twig->render('habitacionesView.html', Array('habitaciones'=>$habitaciones, 'titulo'=>$tituloWeb, 'contenido'=>$contenidoWeb));
		break;

	case 'ver':
		$habitacion=$mishabitaciones->detalle($_GET['id']);
		$tituloWeb='Detalle de habitacion';

		//Pasar los datos a la vista y renderizar
		echo $twig->render('habitacionView.html', Array('habitacion'=>$habitacion, 'titulo'=>$tituloWeb));
		break;
}



?>