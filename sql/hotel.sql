-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-05-2018 a las 19:02:46
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `hotel`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habitaciones`
--

CREATE TABLE `habitaciones` (
  `idHabitacion` int(11) NOT NULL,
  `nombreHabitacion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `plantaHabitacion` int(11) NOT NULL,
  `disponibleHabitacion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `imagenHabitacion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `otro` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `habitaciones`
--

INSERT INTO `habitaciones` (`idHabitacion`, `nombreHabitacion`, `plantaHabitacion`, `disponibleHabitacion`, `imagenHabitacion`, `otro`) VALUES
(1, 'angelus', 1, 'libre', '', ''),
(2, 'habitacion2', 1, 'libre', '', ''),
(3, 'habitacion 3', 1, 'libre', '', ''),
(4, 'habitacion4', 1, 'libre', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagenes`
--

CREATE TABLE `imagenes` (
  `idImagen` int(11) NOT NULL,
  `ficheroImagen` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `textoImagen` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `idHabitacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `imagenes`
--

INSERT INTO `imagenes` (`idImagen`, `ficheroImagen`, `textoImagen`, `idHabitacion`) VALUES
(1, '1.jpg', 'Texto de imagen 1', 1),
(2, '2.jpg', 'texto de habitacion 2', 1),
(3, '3.jpg', 'texto de habitacion 3', 1),
(4, '4.jpg', 'texto de imagen 4', 1),
(5, '5.jpg', '', 2),
(6, '6.jpg', '', 3),
(7, '7.jpg', '', 4),
(8, '8.jpg', '', 4),
(9, '9.jpg', '', 4),
(10, '10.jpg', '', 4),
(11, '11.jpg', '', 4),
(12, '12.jpg', '', 4);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `habitaciones`
--
ALTER TABLE `habitaciones`
  ADD PRIMARY KEY (`idHabitacion`);

--
-- Indices de la tabla `imagenes`
--
ALTER TABLE `imagenes`
  ADD PRIMARY KEY (`idImagen`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `habitaciones`
--
ALTER TABLE `habitaciones`
  MODIFY `idHabitacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `imagenes`
--
ALTER TABLE `imagenes`
  MODIFY `idImagen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
