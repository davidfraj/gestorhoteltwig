<?php  
//fichero models/habitacionModel.php

class Habitacion{

public $id;
public $nombre;
public $planta;
public $disponibilidad;
public $imagen;
public $imagenes;
public $conexion;

public function __construct($registro){
	$this->nombre=$registro['nombreHabitacion'];
	$this->planta=$registro['plantaHabitacion'];
	$this->disponibilidad=$registro['disponibleHabitacion'];
	$this->imagen=$registro['imagenHabitacion'];
	$this->id=$registro['idHabitacion'];
	$this->imagenes=[];
	$this->conexion=Conexion::conectar();

	$sql="SELECT * FROM imagenes WHERE idHabitacion=$this->id";
	$consulta=$this->conexion->query($sql);
	while($registro=$consulta->fetch_array()){
		$this->imagenes[]=new ImagenHabitacion($registro);	
	}

}

}

?>