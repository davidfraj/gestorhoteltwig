<?php  
class Habitaciones{

	private $conexion;
	public $habitaciones;

	public function __construct(){
		$this->habitaciones=[];
		$this->conexion=Conexion::conectar();
	}

	public function listado(){
		$sql="SELECT * FROM habitaciones";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->habitaciones[]=new Habitacion($registro);
		}
		return $this->habitaciones; //Array de Habitaciones
	}

	public function detalle($id){
		$sql="SELECT * FROM habitaciones WHERE idHabitacion=$id";
		$consulta=$this->conexion->query($sql);
		$registro=$consulta->fetch_array();
		$habitacion=new Habitacion($registro);
		return $habitacion;
	}

}



?>