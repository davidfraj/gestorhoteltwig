<?php  
//Require de datos de BBDD (lo he cogido del proyecto noticiasmvc)
require('includes/config.php');
require('includes/conexion.class.php');

//Añadimos nuestro autoload de vendor
require('vendor/autoload.php');

//Le decimos donde van a ir las plantillas de twig
$loader = new Twig_Loader_Filesystem('views/');

//ESTO PARA DESARROLLO
$twig = new Twig_Environment($loader);

//ESTO PARA PRODUCCION
//$twig = new Twig_Environment($loader, array('cache' => 'cache/'));

//Recojo el controlador que quiero llamar
if(isset($_GET['contr'])){
	$contr=$_GET['contr'];
}else{
	$contr='habitacionesController.php';
}

//Agregamos la variable global a twig 
//NOTA: queda pendiente de explicar
//$twig->addGlobal('contr', $contr);

//Tengo que LLAMAR al controlador que quiero hacer funcionar
require('controllers/'.$contr);

?>